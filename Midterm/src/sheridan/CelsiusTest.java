package sheridan;
//joshua clark 991516472
import static org.junit.Assert.*;

import org.junit.Test;

public class CelsiusTest {

	@Test
	public void testFromFahrenheitRegular() {
		int results = Celsius.fromFahrenheit(75);
		assertTrue(results > 10);
	}
	@Test
	public void testFromFahrenheitException() {
		int results = Celsius.fromFahrenheit(0);
		assertFalse(results > 0);
	}
	@Test
	public void testFromFahrenheitBoundaryIn() {
		int results = Celsius.fromFahrenheit(75);
		assertTrue(results >= 23);
	}
	@Test
	public void testFromFahrenheitBoundaryOut() {
		int results = Celsius.fromFahrenheit(75);
		assertFalse(results == 75);
	}

}
