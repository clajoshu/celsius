package sheridan;
//Midterm Exam By Joshua Clark
public class Celsius {
	
	public static int fromFahrenheit(int temp) {
		int celsius = ((5 *(temp - 32)) / 9);
		int roundedCel = (int) Math.ceil(celsius);
		return roundedCel;
	}
}

